
#INSTALANDO GIT

sudo apt install -y git

#INSTALANDO ZSH

sudo apt install -y zsh

#PARA VM XUBUNTU NO VIRTUALBOX

sudo apt-get install build-essential gcc make perl dkms

#INSTALANDO O KUBECTL

curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x kubectl && mv kubectl /usr/local/bin/

https://kubernetes.io/docs/tasks/tools/install-minikube/

#INSTALANDO O AWS CLI

sudo apt install awscli

#Instala o Docker. Adiciona ao sistema a chave GPG oficil do repo do Docker

sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

#Adiciona o repo do Docker às dontes do APT

sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'

#Atualiza o Banco de Dados com os pacotes do Docker

sudo apt-get update

#Certificar o uso do repo do Docker

sudo apt-cache policy docker-engine

#Instala o Docker
sudo apt-get install -y docker-engine

#INSTALANDO O MINIKUBE

curl -Lo minikube https://github.com/kubernetes/minikube/releases/download/v0.28.0/minikube-linux-amd64
chmod +x minikube && mv minikube /usr/local/bin/

#INICIANDO O MINIKUBE

minikube start

#CRIANDO DEPLOYMENT

kubectl run nginx --image nginx 

#INSTALANDO HELM

sudo snap install helm --classic
