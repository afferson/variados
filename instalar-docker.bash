#Atualiza o Banco de Dados de pacotes
sudo apt-get update

#Instala o Docker. Adiciona ao sistema a chave GPG oficil do repo do Docker
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

#Adiciona o repo do Docker às dontes do APT
sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'

#Atualiza o Banco de Dados com os pacotes do Docker
sudo apt-get update

#Certificar o uso do repo do Docker
apt-cache policy docker-engine

#Instala o Docker
sudo apt-get install -y docker-engine

