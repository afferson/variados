import boto3
import requests

print("discovering rabbitmq instances")
ec2 = boto3.resource('ec2', 'sa-east-1')
ec2_client = boto3.client('ec2', 'sa-east-1')

DOT = '.'
DASH = '-'
FILE_MODE = 'a'
BREAK_LINE = '\n'
HOST_FILE = '/etc/hosts'

instance_id = requests.get("http://169.254.169.254/latest/meta-data/instance-id")
print("current instance-id: %s" %(instance_id.text))
cluster_nodes = []
result = ec2.Instance(instance_id.text)
tags_f = {}
for tag in result.tags:
   tags_f.update({tag['Key']:tag['Value']})

filter =  [{'Name':'tag:aws:cloudformation:stack-id', 'Values':[tags_f["aws:cloudformation:stack-id"]]},
	   {'Name': 'instance-state-name', 'Values': ['running']}]

ec2_cluster_instances = instances = ec2.instances.filter(Filters=filter)

for instance in ec2_cluster_instances:
    node_ip = instance.private_ip_address
    hostname = "ip.%s" %(node_ip)
    node = "%s %s" %(node_ip, hostname.replace(DOT, DASH))
    print(node)
    cluster_nodes.append(node)

host_file = open(HOST_FILE, FILE_MODE)

for cluster_node in cluster_nodes:
    host_file.write(BREAK_LINE)
    host_file.write(cluster_node)

host_file.close()