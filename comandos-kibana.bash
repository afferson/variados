curl -XGET 'https://vpc-logs-easynvest-s5rrbocnmi7pidjbpenenv5jza.sa-east-1.es.amazonaws.com/_snapshot/cs-automated/_all?pretty'

#RECUPERA SNAPSHOT QUANDO NÂO HÁ INDEX CRIADO

PUT https://search-easynvest-logs-pj5srg2z535r2qfdsbqkk7nzwy.sa-east-1.es.amazonaws.com/_snapshot/my-snapshot-repo
{
  "type": "s3",
  "settings": {
    "bucket": "https://s3.console.aws.amazon.com/s3/buckets/easy-logs/elasticsearch",
    "region": "sa-east-1",
    "role_arn": "arn:aws:iam::827461279864:role/aws-service-role/es.amazonaws.com/AWSServiceRoleForAmazonElasticsearchService"
  }
}

#RECUPERA SNAPSHOT INPUTANDO INFORMAÇÕES EM UM NOVO INDEX

POST -H 'Content-Type: application/json' 'elasticsearch-domain-endpoint/_snapshot/cs-automated/2019-02-20t00-15-40.0ff0c42f-0f8a-48b2-8a73-ac872d328255/_restore' -d '
{
  "indices" : "servers",
  "ignore_unavailable" : true,
  "include_global_state" : true,
  "rename_pattern" : "servers",
  "rename_replacement" : "servers_old"
}

curl -XPOST 'elasticsearch-domain-endpoint/_snapshot/cs-automated/2019-02-20t00-15-40.0ff0c42f-0f8a-48b2-8a73-ac872d328255/_restore'
{
    "indices": "servers", 
    "rename_pattern": "servers_(.+)", 
    "rename_replacement": "server_old" 
}

#MOSTRA INFORMAÇÕES DE RECUPERAÇÃO DE INDEX

GET servers/_recovery?human

#VERIFICA STATUS DE RECUPERAÇÃO DO CLUSTER

GET /_recovery?human

#LISTA 

curl -XGET 'elasticsearch-domain-endpoint/_snapshot/cs-automated/_all?pretty'

curl -XDELETE 'elasticsearch-domain-endpoint/_all'

curl -XPOST 'elasticsearch-domain-endpoint/_snapshot/cs-automated/2019-02-20t00-15-40.0ff0c42f-0f8a-48b2-8a73-ac872d328255/_restore' -d '{"indices": "servers"}' -H 'Content-Type: application/json'

curl -XPOST 'elasticsearch-domain-endpoint/_snapshot/cs-automated/2019-02-20t00-15-40.0ff0c42f-0f8a-48b2-8a73-ac872d328255/_restore' -d '{"indices": "servers"}' -H 'Content-Type: application/json'

curl -XPOST 'elasticsearch-domain-endpoint/_snapshot/cs-automated/2019-02-20t00-15-40.0ff0c42f-0f8a-48b2-8a73-ac872d328255/_restore'

# CRIAR INDEX COM CONFIGS DE SHARDS E REPLICAS

PUT index_name
{
      "settings" : {
        "number_of_shards" : 26,
        "number_of_replicas" : 1
      }
}
